#!/usr/bin/env bash

#genmon script for displaying the NowTime
#displays NowDate and NowTime on the tooltip

NowTime=$(echo "\ue383 ")
NowDate=$(echo "\uf073 ")
NowDate+=`date '+ %d %B %A %H:%M'`
NowTime+=`date '+%H:%M'`

# Panel
DisplayPanel="<txt>"
DisplayPanel+="${NowTime}"
DisplayPanel+="</txt>"

# Tooltip
DisplayTooltip="<tool>"
DisplayTooltip+="${NowDate}"
DisplayTooltip+="</tool>"

# Panel Print
echo -e "${DisplayPanel}"

# Tooltip Print
echo -e "${DisplayTooltip}"
