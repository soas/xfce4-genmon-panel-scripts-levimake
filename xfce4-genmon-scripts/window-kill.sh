#!/usr/bin/env bash

#genmon script for KILLING the active window application (all windows)

style="<css>.genmon_valuebutton {
#     background-color: red;
    border:3px solid;
    padding: 5px;
#     min-width: 20px;
    }</css>"

ScriptIcon="☠️"
ScriptIconDisabled="󰇜"  ## shown when Desktop is active

TitleLong=$(xdotool getactivewindow getwindowname)
# WindowsOpenNumber=$(wmctrl -l | wc -l)

DisplayPanel="<txt>"
DisplayPanel+="<span>"
if [ "${TitleLong}" != "Desktop" ];
    then
        echo $style
        DisplayPanel+="$ScriptIcon"
else
    ## do not show if active window is Desktop (to avoid crash)
    DisplayPanel+="$ScriptIconDisabled"
fi

DisplayPanel+="</span>"
DisplayPanel+="</txt>"


if [ "${TitleLong}" != "Desktop" ];
    then
        ## do not show if active window is Desktop (to avoid crash)
        DisplayPanel+="<txtclick>xdotool windowkill `xdotool getactivewindow`</txtclick>"
fi
echo -e "${DisplayPanel}"

DisplayTooltip="<tool>"
DisplayTooltip+="☠️ Kill active application (all windows) ☠️ \r$TitleLong"
DisplayTooltip+="</tool>"

echo -e "${DisplayTooltip}"
