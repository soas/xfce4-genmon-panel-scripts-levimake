#!/usr/bin/env bash

#genmon-script for opening the logout.. menu ;-)

IconPower=$(echo "\uf011")

# Panel
DisplayPanel="<txt>"
DisplayPanel+="${IconPower}"
DisplayPanel+="</txt>"
######################
#### Will log you OUT with NO warning or confirmation!!!
#### uncomment below line if you want this:
# DisplayPanel+="<txtclick>xfce4-session-logout</txtclick>"
######################

# Tooltip
DisplayTooltip="<tool>"
DisplayTooltip+="${NULL_VALUE}"  #to hide the tooltip
DisplayTooltip+="</tool>"

# Panel Print
echo -e "${DisplayPanel}"

# Tooltip Print
echo -e "${DisplayTooltip}"
