#!/usr/bin/env bash

#genmon script for showing desktop
IconShowDesktop=$(echo "\uf6c3")

DisplayPanel="<txt>"
DisplayPanel+="<span weight='Regular' fgcolor='White'>"
DisplayPanel+="${IconShowDesktop} "
DisplayPanel+="</span>"
DisplayPanel+="</txt>"
DisplayPanel+="<txtclick>xdotool key ctrl+alt+d</txtclick>"
echo -e "${DisplayPanel}"

DisplayTooltip="<tool>"
DisplayTooltip+="${NULL_VALUE}" #to hide the tooltip
DisplayTooltip+="</tool>"

echo -e "${DisplayTooltip}"
