#!/bin/bash
TodoFile=~/.rofi_todos

if [[ ! -a "${TodoFile}" ]]; then
    touch "${TodoFile}"
fi

function add_todo() {
    echo -e "`date +"%B %d %H:%M"` $*" >> "${TodoFile}"
}

function remove_todo() {
    sed -i "/^${*}$/d" "${TodoFile}"
}

function get_todos() {
    echo "$(cat "${TodoFile}")"
}

if [ -z "$@" ]; then
    get_todos
else
    Line=$(echo "${@}" | sed "s/\([^a-zA-Z0-9]\)/\\\\\\1/g")
    LineUnescaped=${@}
    if [[ $LineUnescaped == +* ]]; then
        LineUnescaped=$(echo $LineUnescaped | sed s/^+//g |sed s/^\s+//g )
        add_todo ${LineUnescaped}
    else
        IsMatch=$(grep "^${LineUnescaped}$" "${TodoFile}")
        if [[ -n "${IsMatch}" ]]; then
            remove_todo ${LineUnescaped}
        fi
    fi
    get_todos
fi
