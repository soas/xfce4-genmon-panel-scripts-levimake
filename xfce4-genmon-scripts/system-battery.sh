#!/usr/bin/env bash

#genmon script for the battery icon

readonly BatteryVoltage=$(awk '{$1 = $1 / 1000000; print $1}' /sys/class/power_supply/BAT*/voltage_now)
readonly BatteryLevel=$(awk '{print $1}' /sys/class/power_supply/BAT*/capacity)
readonly NowTime_UNTIL=$(acpi | awk '{print $5}' | awk 'NR==2')

#Set your custom low_battery level
BatteryLow=25;

DisplayPanel=
if [ "${BatteryLevel}" -lt "${BatteryLow}" ]; then                # battery is too low ; don't make me appear ; charge it before reaching this condition
  IconBattery=$(echo "\uf244")
elif [ "${BatteryLevel}" -ge 25 ] && [ "${BatteryLevel}" -lt 50 ]; then # if battery is charged between 25 and 50
  IconBattery=$(echo "\uf243")
elif [ "${BatteryLevel}" -ge 50 ] && [ "${BatteryLevel}" -lt 60 ]; then # if battery is half charged
  IconBattery=$(echo "\uf242")
elif [ "${BatteryLevel}" -ge 60 ] && [ "${BatteryLevel}" -lt 95 ]; then # if battery is charged more than 60%
  IconBattery=$(echo "\uf241")
else                                                          # if battery is full charged
  IconBattery=$(echo "\uf240")
fi

if acpi -a | grep -i "on-line" &> /dev/null; then # if AC adapter is online
    IconBattery+=$(echo " \uf0e7")                # adds a voltage icon next to battery
fi

if hash xfce4-power-manager-settings &> /dev/null; then
  DisplayPanel+="<txtclick>xfce4-power-manager-settings</txtclick>"     # clicking on the icon opens XFCE power manager
fi

DisplayPanel+="<txt>"
if acpi -a | grep -i "off-line" &> /dev/null; then   # if AC adapter is offline
  if [ "${BatteryLevel}" -lt ${BatteryLow} ]; then       # if battery is less than $BatteryLow
    DisplayPanel+="<span weight='Bold' fgcolor='Red'>"       # make the text bold red
  elif [ "${BatteryLevel}" -gt 75 ]; then
    DisplayPanel+="<span foreground=\"#cdffcd\">"  #text appears green if the charge is above 75
  else
    DisplayPanel+="<span  fgcolor='White'>" # make the text white for normal states
  fi
else # if the battery is getting charged
  DisplayPanel+="<span  foreground=\"#cdffcd\">" # text is green while getting charged
fi
DisplayPanel+="${IconBattery} "
DisplayPanel+="</span>"
DisplayPanel+="<span foreground=\"#fff\">"
DisplayPanel+="${BatteryLevel}%"
DisplayPanel+="</span>"
DisplayPanel+="</txt>"


# Tooltip
DisplayTooltip="<tool>"

if acpi -a | grep -i "off-line" &> /dev/null; then # if AC adapter is offline
  if [ "${BatteryLevel}" -eq 100 ]; then # if battery is fully charged
    DisplayTooltip+="└─ Voltage: ${BatteryVoltage} V"
  else
    DisplayTooltip+="└─ Remaining NowTime: ${NowTime_UNTIL}"      # shows the remaining NowTime on battery
  fi
elif acpi -a | grep -i "on-line" &> /dev/null; then # if AC adapter is online
  if [ "${BatteryLevel}" -eq 100 ]; then # if battery is fully charged
    DisplayTooltip+="└─ Voltage: ${BatteryVoltage} V"
  else 
    DisplayTooltip+="└─ NowTime to fully charge: ${NowTime_UNTIL}"    # shows remaining NowTime to charge
  fi
else # if battery is in unknown state (no battery at all, throttling, etc.)
  DisplayTooltip+="└─ Voltage: ${BatteryVoltage} V"
fi
DisplayTooltip+="</tool>"

# Panel Print
echo -e "${DisplayPanel}"

# Tooltip Print
echo -e "${DisplayTooltip}"
