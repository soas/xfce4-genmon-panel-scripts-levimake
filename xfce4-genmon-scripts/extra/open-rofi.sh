#sidebar menu with search icon

IconMenu=$(echo "\uf002")

# Panel
DisplayPanel="<txt>"
DisplayPanel+="${IconMenu}"
DisplayPanel+="</txt>"
DisplayPanel+="<txtclick>rofi -combi-modi drun -theme sidetab  -show drun -display-drun "Apps" -icon-theme "Papirus" -show-icons</txtclick>"

# Tooltip
DisplayTooltip="<tool>"
DisplayTooltip+="${NULL_VALUE}" #to hide the tooltip
DisplayTooltip+="</tool>"

# Panel Print
echo -e "${DisplayPanel}"

# Tooltip Print
echo -e "${DisplayTooltip}"
