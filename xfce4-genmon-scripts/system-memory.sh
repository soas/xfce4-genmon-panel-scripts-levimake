#!/usr/bin/env bash
# Dependencies: bash>=3.2, coreutils, file, gawk

# Calculate RAM values
readonly RamTotal=$(free -b | awk '/^[Mm]em/{$2 = $2 / 1073741824; printf "%.2f", $2}')
readonly RamUsed=$(free -b | awk '/^[Mm]em/{$3 = $3 / 1073741824; printf "%.2f", $3}')
readonly RamFree=$(free -b | awk '/^[Mm]em/{$4 = $4 / 1073741824; printf "%.2f", $4}')
readonly RamShared=$(free -b | awk '/^[Mm]em/{$5 = $5 / 1073741824; printf "%.2f", $5}')
readonly RamCached=$(free -b | awk '/^[Mm]em/{$6 = $6 / 1073741824; printf "%.2f", $6}')
readonly RamAvailable=$(free -b | awk '/^[Mm]em/{$7 = $7 / 1073741824; printf "%.2f", $7}')

# Swap Values
readonly SWP_RamTotal=$(free -b | awk '/^[Ss]wap/{$2 = $2 / 1073741824; printf "%.2f", $2}')
readonly SWP_RamUsed=$(free -b | awk '/^[Ss]wap/{$3 = $3 / 1073741824; printf "%.2f", $3}')
readonly SWP_RamFree=$(free -b | awk '/^[Ss]wap/{$4 = $4 / 1073741824; printf "%.2f", $4}')

IconMemory=$(echo "\uf85a")


# Panel
DisplayPanel+="<txt>"
DisplayPanel+="${IconMemory} "
DisplayPanel+="${RamUsed}"
# DisplayPanel+="／"
# DisplayPanel+="${RamTotal} GB"
DisplayPanel+="G"
DisplayPanel+="</txt>"

# Tooltip
DisplayTooltip="<tool>"
DisplayTooltip+="┌ RAM\n"
DisplayTooltip+="├─ Used\t\t${RamUsed} GB\n"
DisplayTooltip+="├─ Free\t\t\t${RamFree} GB\n"
DisplayTooltip+="├─ Shared\t\t${RamShared} GB\n"
DisplayTooltip+="├─ Cache\t\t${RamCached} GB\n"
DisplayTooltip+="└─ Total\t\t${RamTotal} GB"
DisplayTooltip+="\n\n"
DisplayTooltip+="┌ SWAP\n"
DisplayTooltip+="├─ Used\t\t${SWP_RamUsed} GB\n"
DisplayTooltip+="├─ Free\t\t\t${SWP_RamFree} GB\n"
DisplayTooltip+="└─ Total\t\t${SWP_RamTotal} GB"
DisplayTooltip+="</tool>"

# Panel Print
echo -e "${DisplayPanel}"

# Tooltip Print
echo -e "${DisplayTooltip}"
