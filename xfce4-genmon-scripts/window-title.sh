#!/usr/bin/env bash

#genmon script for displaying the active window title on the panel

h1=$(tput setaf 3)   ## highlight yellow
h2=$(tput setaf 4)   ## highlight blue
h0=$(tput sgr0)     ## highlight off

## set the max length of content
TitleMax="66"
#     echo -e "$h1\ee  TitleMax$h0 is $h2 $TitleMax   $h0"

TitleLong=$(xdotool getactivewindow getwindowname)
#     echo -e "$h1\ee  TitleLong $h0 is $h2 $TitleLong   $h0"


TitleLength=$(echo $TitleLong | wc -c)
#     echo -e "$h1\ee  TitleLength $h0 is $h2 $TitleLength   $h0"

## Edit the obtained window title:
##     1. remove firefox from name
##     2. enforce TitleMax

TitleShort=$(echo $TitleLong \
    |  sed -E 's/( Firefox Developer Edition)/FF/' \
    | cut -c -$TitleMax)
#     echo -e "$h1\ee  TitleShort $h0 is $h2 $TitleShort   $h0"

##  2 ways to remove long FF window titles:
    # echo ${TitleShort% Firefox Developer Edition}
    # echo ${win} |  sed -E 's/( Firefox Developer Edition)/FF/'
    # Manipulating Strings —FF

# WindowsOpenNumber=$(wmctrl -l | wc -l)
#     echo -e "$h1\ee  WindowsOpenNumber $h0 is $h2 $WindowsOpenNumber   $h0"

DisplayPanel="<txt>"
DisplayPanel+="<span weight='Regular' fgcolor='White'>"
DisplayPanel+="${TitleShort}"
if [ "${TitleLength}" -gt $TitleMax ]; then
    DisplayPanel+="..."
#     echo -e "-----$h1\ee  TitleLength $h0 -gt $h2 TitleMax ($TitleMax)   $h0 == TRUE"
fi
DisplayPanel+="</span>"
DisplayPanel+="</txt>"

DisplayPanel+="<txtclick>rofi -combi-modi window -show window -icon-theme "Papirus" -show-icons</txtclick>"

echo -e "${DisplayPanel}"

DisplayTooltip="<tool>"
DisplayTooltip+="$TitleLong"
DisplayTooltip+="</tool>"

echo -e "${DisplayTooltip}"
